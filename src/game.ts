import { App, TiledLevel, UILabel } from 'squaredjs'
import Logo from './entities/logo'

export default class Game extends App {

  public logo!: Logo
  public label!: UILabel

  constructor() {
    // We call the parent constructor, giving it the html element's id
    super('gameCanvas', { scale: 8, width: 16 * 8, height: 9 * 8 })
  }

  public update(delta: number): void {
    super.update(delta)
  }
}

// Instantiate the game instance
const game = new Game()

// "Prepare" the game: preloads required assets such as textures, fonts and sounds
game.prepare()
  .then(async () => {
    // Create an empty Level
    const level = new TiledLevel()

    // Add it to the scene hierarchy
    game.rootContainer.addChild(level)

    // Add an entity
    game.label = new UILabel('SquaredJS\nIs Awesome\n:D', 16 * 4, 10, game.ui, { fontFamily: 'm5x7', align: 'center' })
    game.logo = new Logo({ name: 'logo', x: 50, y: 10 })
    game.addEntityToScene(game.logo, level.entitiesLayers[0])

    console.log('Ready!')
  })
